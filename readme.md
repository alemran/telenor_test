
## About Project
This project for storing data to cache. I have used Laravel Framework and file cache with TTL. I have created three endpoints API. 
 
- I have created a store name keysCollection for collecting all keys those are stored.
  When need to show all stored data then collect all keys and show all key's value from cache with kay-value pair as Array.
- I have used Job/Queue for better performance:
```php
        CacheKeysUpdater::dispatch()->delay(now()->addSeconds(1)); // for updating valid cache keys collection
        CacheTTLReset::dispatch()->delay(now()->addSeconds(1)); // for reset TTL of valid Cache
          CacheAddKeysToCollection::dispatch($keys)->delay(now()->addSeconds(1)); // for adding keys to key's collection
```
  
```php

    /** 
     * this function for  adding key to key's collection
     * @param $keys
     * @return void
     */     
      public static function addKeyToKeysCollection($keys)
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];

        foreach ($keys as $k => $v) {

            if (!in_array($v, $collections)) {
                array_push($collections, $v);
            }
        }

        Cache::forever('keysCollection', $collections);
    }
	
	
    /** 
     * this function for collecting all key , value pair
     * @return array
     * @throws \Exception
     */
	 public static function getAllData()
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];
        $newData = [];
        foreach ($collections as $key => $value) {
            $singleData = cache($value);
            if ($singleData) {
                $newData[$value] = $singleData;
            }
        }
        return $newData;
    }
	
	 /** 
     * this function for collecting key based data
     * @param $keys
     * @return array
     * @throws \Exception
     */
 public static function keyBasedData($keys)
    {
        $newData = [];
        foreach ($keys as $k => $v) {
            $singleData = cache($v);
            if ($singleData) {
                $newData[$v] = $singleData;
            }
        }
        return $newData;
    }

```
  
- Update all keys collection with Job/Queue (check all valid key from cache and unset all expired key). 


```php

    /** 
     * this function for  updating  keys collection with checking all key' s value exist or expired
     * @return void
     * @throws \Exception
     */
    public static function updateKeysCollection()
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];

        foreach ($collections as $key => $value) {
            if (!cache($value)) {
                unset($collections[$key]);
            }
        }
        Cache::forever('keysCollection', $collections);
    }

```

- Update TTL when get api called

```php

  /** 
     * this function for  reset all valid cache TTL
     * @return void
     * @throws \Exception
     */
  public static function resetCacheTTL()
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];

        foreach ($collections as $key => $value) {
            $cacheData = cache($value);
            if (!$cacheData) {
                unset($collections[$key]);
            } else {
                cache([$value => $cacheData], now()->addMinute(5));
            }
        }
        Cache::forever('keysCollection', $collections);
    }

```


## /values (get)
By default this endpoint return all keys value pair.
if we pass keys=key1,key2 .... like get param, return all provided keys key-value data;

{ "book": "Bangladesh", "page": "500" }

- [Update TTL with every request] 

## /values (Post)
for store key-value we can pass key-value array/object 
Post Data format: { "book": "Bangladesh", "page": "500" }

## /values (Patch)
for updating  value of cache  
 
  
  
  
  
  
## SQL Test

- Write a SQL query to fetch ALL appointments on October,
   ordered by slot_date_time in increasing order.

```sql
SELECT appointments.patient_name, doctors.doctor_name,CONCAT(slots.date,' ', slots.time) as slot_date_time from appointments 
INNER JOIN doctors ON doctors.id = appointments.doctor_id
INNER JOIN slots ON  slots.id = appointments.slot_id
WHERE appointments.deleted_at IS NULL ORDER BY slot_date_time ASC;
```
  
  
- Write a SQL query to fetch the doctor with highest number of appointments

```sql

SELECT doctors.doctor_name, count(appointments.`doctor_id`) as appointment_count FROM `appointments`
INNER JOIN doctors ON doctors.id = appointments.doctor_id
GROUP BY appointments.`doctor_id` Order BY  appointment_count  DESC LIMIT 0,1

--OR

--create view
CREATE VIEW doctor_total_appointment AS
SELECT doctors.doctor_name, count(appointments.doctor_id) as totalAppointment
FROM appointments
INNER JOIN doctors ON doctors.id = appointments.doctor_id
WHERE appointments.`deleted_at` IS NULL
GROUP BY appointments.`doctor_id`

--get max appointment doctor
SELECT `doctor_name`, `totalAppointment` as appointment_count FROM `doctor_total_appointment`   ORDER BY `totalAppointment` DESC LIMIT 0, 1



```
  
  
    
  
- Write a SQL query to show list of doctors with their total appointment durations in decreasing

```sql

SELECT doctors.doctor_name,SUM(slots.duration) as total_duration FROM appointments
INNER JOIN doctors ON doctors.id = appointments.doctor_id
INNER JOIN slots ON slots.id = appointments.slot_id
GROUP BY appointments.doctor_id  ORDER BY total_duration DESC;


```
  
  
  
## About

 AL EMRAN. [Website](http://alemran.me),[Github](http://github.com/emrancu).
