<?php

namespace App\Http\Controllers\Cache;

use App\Http\Controllers\Controller;
use App\Jobs\CacheKeysUpdater;
use App\Jobs\CacheAddKeysToCollection;
use App\Jobs\CacheTTLReset;
use App\utilities\CacheDataControl;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CacheDataController extends Controller
{


    /**
     * get all with key value pair
     * can be access specific keys value with ?keys=key1,key2... or without param
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getAll(Request $request)
    {

        $values = [];

        if ($request->keys) {
            $keys = explode(",", $request->keys);
            $values = CacheDataControl::keyBasedData($keys);
        } else {
            $values = CacheDataControl::getAllData();
        }

        /**
         * call Job/queue
         */
        CacheKeysUpdater::dispatch()->delay(now()->addSeconds(1)); // for updating valid cache keys collection
        CacheTTLReset::dispatch()->delay(now()->addSeconds(1)); // for reset TTL of valid Cache

        return response()->json($values, 200);
    }


    /**
     * set cache with requested key & value
     * @param Request $request
     * @return JsonResponse
     */
    public function setValue(Request $request)
    {

        $data = $request->all();
        $keys = array_keys($data);
        try {
            foreach ($data as $key => $value) {
                cache([$key => $value], now()->addMinute(5));
            }
        } catch (\Exception $e) {
            return response()->json("Something went wrong ", 200);
        }

        /**
         * call Job/queue
         */
        CacheKeysUpdater::dispatch()->delay(now()->addSeconds(1)); // for updating valid cache keys collection
        CacheAddKeysToCollection::dispatch($keys)->delay(now()->addSeconds(1)); // for adding keys to key's collection

        return response()->json("Value Stored successfully", 200);

    }


    /**
     * updated cache value with requested data (Array) with patch request (x-www-form-urlencoded data).
     * @param Request $request
     * @return JsonResponse
     */
    public function updateValue(Request $request)
    {
        $data = $request->all();
        $keys = array_keys($data);
        $expiry = false;
        try {
            foreach ($data as $key => $value) {
                $data = cache($key);
                if ($data) {
                    cache([$key => $value], now()->addMinute(5));
                } else {
                    $expiry = true;
                }
            }
        } catch (\Exception $e) {
            return response()->json("Something went wrong ", 200);
        }

        /**
         * call Job/queue
         */
        CacheKeysUpdater::dispatch()->delay(now()->addSeconds(1)); // for updating valid cache keys collection

        $responseMessage = "Data updated successfully" . ($expiry ? " and some key already expired" : '');
        return response()->json($responseMessage, 200);

    }


}
