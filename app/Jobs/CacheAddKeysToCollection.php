<?php

namespace App\Jobs;

use App\utilities\CacheDataControl;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CacheAddKeysToCollection implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $keys;

    /**
     * Create a new job instance.
     *
     * @param $keys
     */
    public function __construct($keys)
    {
       $this->keys = $keys;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        CacheDataControl::addKeyToKeysCollection($this->keys) ;
    }
}
