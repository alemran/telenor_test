<?php


namespace App\utilities;


use Illuminate\Support\Facades\Cache;

class CacheDataControl
{


    /**
     * we store keysCollection with all keys those was set
     * this function for collecting all key , value pair
     * @param $keys
     * @return array
     * @throws \Exception
     */
    public static function keyBasedData($keys)
    {
        $newData = [];
        foreach ($keys as $k => $v) {
            $singleData = cache($v);
            if ($singleData) {
                $newData[$v] = $singleData;
            }
        }
        return $newData;
    }

    /**
     * we store keysCollection with all keys those was set
     * this function for collecting all key , value pair
     * @return array
     * @throws \Exception
     */
    public static function getAllData()
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];
        $newData = [];
        foreach ($collections as $key => $value) {
            $singleData = cache($value);
            if ($singleData) {
                $newData[$value] = $singleData;
            }
        }
        return $newData;
    }


    /**
     * we store keysCollection with all keys those was set
     * this function for  adding key to key's collection
     * @param $keys
     * @return void
     */
    public static function addKeyToKeysCollection($keys)
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];

        foreach ($keys as $k => $v) {

            if (!in_array($v, $collections)) {
                array_push($collections, $v);
            }
        }

        Cache::forever('keysCollection', $collections);
    }


    /**
     * we store keysCollection with all keys those was set
     * this function for  updating  keys collection with checking all key' s value exist or expired
     * @return void
     * @throws \Exception
     */
    public static function updateKeysCollection()
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];

        foreach ($collections as $key => $value) {
            if (!cache($value)) {
                unset($collections[$key]);
            }
        }
        Cache::forever('keysCollection', $collections);
    }


    /**
     * we store keysCollection with all keys those was set
     * this function for  reset all valid cache TTL
     * @return void
     * @throws \Exception
     */
    public static function resetCacheTTL()
    {
        $storedCollection = Cache::get('keysCollection');
        $collections = $storedCollection ? $storedCollection : [];

        foreach ($collections as $key => $value) {
            $cacheData = cache($value);
            if (!$cacheData) {
                unset($collections[$key]);
            } else {
                cache([$value => $cacheData], now()->addMinute(5));
            }
        }
        Cache::forever('keysCollection', $collections);
    }


}
